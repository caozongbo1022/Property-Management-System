from django.apps import AppConfig


class AppProMgrConfig(AppConfig):
    name = 'app_pro_mgr'
